# 442 - RAPPORT DE PROJET - JEU DE QUILLES
<center> **Mélanie PIETRI - Sébastien QUISTREBERT** </center>  

## Présentation du projet 
Le but du projet est de créer un jeu de quilles vu du dessus qui utilise l'écran d'affichage de deux cartes STM32. Sur une carte seront affichées les quilles et les informations relatives au jeu et sur l'autre carte on affichera la balle et l'interface qui permet de la tirer. 
La communication entre les deux cartes s'effectuera à l'aide d'une liaison série. 
  
<center> <img src="images/schema-deux-cartes.png" alt="Schéma de principe du jeu" width="500"/> </center>
<center> *Figure 1 : Schéma de principe du jeu* </center>  
<!-- Compléter le lien vers le git où est l'image -->
  
## Problématiques amenées par ce projet 
Plusieurs problèmatiques sont posées par ce projet : 
* Gestion des différentes tâches
* Étude de la physique du problème
* Format des données 
* Gestion de la transmission des données entre les deux cartes 

On doit envoyer position, sens et vitesse de la balle. On envoie ces données au moment ou la balle traverse l'écran. La messagerie permet d'envoyer 64 bits. On met toutes les variables dans un mot de 64 bits et décoder. 8 premiers bits correspondant à la vitesse suivant x, 8 bits d'après correspondent au signe, 8 bits désignent la vitesse selon y, 8 bits pour le sens selon y. 

## Solutions techniques développées 
### *Gestion des différentes tâches*   
Nous avons crée seulement deux tâches. Une tâche gère le lancement de la balle et une autre les calculs et l'affichage de la balle sur la carte lançant la base et l'autre carte ne possède qu'une tâche. 
Comme une seule tâche tourne à chaque fois, il n'y a pas besoin d'utiliser des MUTEX afin de gérer les conflits d'affichages de plusieurs tâches.

### *Étude de la physique du problème* 
Le joueur peut imposer une vitesse initiale en changeant la longeur de la flèche blanche et une direction initiale en bougeant le joystick afin de choisir l'angle de tir compris entre $0$° et $180$°.

<!-- Compléter avec une image de la position initiale -->

Lorsque la balle touche une paroi (défini par le bord de la carte), celle-ci rebondit et sa vitesse $v_{x}$ est changée en $-v_{x}$ comme on le voit sur la **Figure 2** ci-dessous.   

<!--![Getting Started](https://gitlab.com/melanie-pietri/projet-ii-s2/-/blob/Rapport-de-projet-442.md/schéma-balle.png)-->  
<!--![Getting Started](schéma-balle.png)-->  <!-- Trouver un moyen de réduire la taille de l'image -->
<!--![](./pic/schéma-balle.png =743x489)-->
<center> <img src="images/schema-balle.png" alt="Schéma du rebond de la balle contre une paroi" width="500"/> </center>
<center> *Figure 2 : Schéma du rebond de la balle contre une paroi* </center>  

On ajoute une force de frottement qui permet de ralentir la balle et d'éviter que celle-ci se déplace indéniniment sur l'écran même après avoir heurté une paroi ou une quille. 

### *Format des données*  
Nous avons choisi de travailler avec des coordonnées entières pour la position de la balle et sa vitesse. 

### *Gestion de la transmission des données entre les deux cartes*   
Il y a au total $64$ bits de données à envoyer : $16$ bits pour la position suivant $x$, $16$ bits pour la position suivant $y$, $16$ bits pour la vitesse $v_{x}$ suivant $x$ et $16$ bits pour la vitesse $v_{y}$ suivant $y$. 
La liaison série fonctionnant sur $8$ bits, on envoie les données en les "coupant" en deux. 

La fonction qui gère l'affichage de la balle et les rebonds est assez semblable pour les deux cartes afin d'assurer une bonne coordination. Au début nous avons eu des difficultés lors du passage de la balle d'un écran vers un autre. En effet la balle traversait la première carte mais n'avait plus la bonne trajectoire sur la deuxième carte.  

Nous avons également eu un autre problème : lorsque la balle traversait l'un des deux écrans, elle était bien transmise sur l'autre et une nouvelle balle s'affichait à la frontière, comme on peut le voir sur la **Figure 3**. Lorsque la balle retouchait une balle créee elle effaçait celle-ci. 

<center> <img src="images/probleme-passage-ecran-1-2.JPG" alt="Problème d'affichage de la balle" width="500"/> </center>
<center> *Figure 3 : Problème d'affichage de la balle* </center> 

Lorsque la balle est passée de l'autre côté, la tâche de calcul n'était pas nécessairement finie. On rajoute donc une condition qui affiche la balle uniquement si la balle est bien présente. 

<!--Test formules maths latex : 
$$1 +  \frac{q^2}{(1-q)}+\frac{q^6}{(1-q)(1-q^2)}+\cdots = \prod_{j=0}^{\infty}\frac{1}{(1-q^{5j+2})(1-q^{5j+3})}, \quad\quad \text{for }\lvert q\rvert<1.$$-->

## Intéractions entre les tâches 
On décrit la gestion des différentes tâches sur la **Figure 4** suivante :
<center> <img src="images/schema-interactions-taches.png" alt="Visualisation des intéractions entre les tâches" width="500"/> </center>
<center> *Figure 4 : Visualisation des intéractions entre les tâches* </center>  


## Conclusion technique 
Nous avons réussi à faire fonctionner le déplacement de la balle via le joystick en sélectionnant l'angle et la vitesse initiale de tir. Nous avons mis plus de temps à faire fonctionner la liaison série. 

..